import React, { useContext, useReducer } from 'react';

export const AppContext = React.createContext();
export const useAppContext = () => useContext(AppContext);

const AppContextProvider = ({ state, reducer, children }) => {
  return (
    <AppContext.Provider value={useReducer(reducer, state)}>
      {children}
    </AppContext.Provider>
  );
};
export default AppContextProvider;
