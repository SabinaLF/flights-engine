export const light = {
  colors: {
    primaryBackground: '#EDEDD3',
    secondaryBackground: '#DDDDB7',
    primaryContent: '#F64C72',
    secondaryContent: '#83AF9B',
    secondaryContentLight: '#C0D6CC',
    button: '#FE4365',
  },
  breakpoint: '1080px',
};
export const bright = {
  colors: {
    primaryBackground: '#242582',
    secondaryBackground: '#2F2FA2',
    primaryContent: '#99738E',
    secondaryContent: '#553D67',
    secondaryContentLight: '#A89CB1',
    button: '#F64C72',
  },
  breakpoint: '1080px',
};
export const dark = {
  colors: {
    primaryBackground: '#1A1A1D',
    secondaryBackground: '#4E4E50',
    primaryContent: '#950740',
    secondaryContent: '#6F2232',
    secondaryContentLight: '#B58D96',
    button: '#C3073F',
  },
  breakpoint: '1080px',
};
