import { useEffect } from 'react';
import axios from 'axios';
import { useAppContext } from '../context/app-context';

export const useFetchAirlines = () => {
  const [, dispatch] = useAppContext();
  useEffect(() => {
    const fetchData = async () => {
      dispatch({ type: 'IS_LOADING_AIRLINES', payload: true });
      try {
        const API_KEY = process.env.REACT_APP_API_KEY;
        const res = await axios({
          method: 'get',
          url: `https://recruitment.shippypro.com/flight-engine/api/airlines/all`,
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${API_KEY}`,
          },
        });
        const fetchedAirlines = res.data.data.map((key) => {
          return { id: key.id, name: key.name };
        });
        dispatch({ type: 'FETCH_AIRLINES', payload: fetchedAirlines });
        dispatch({ type: 'IS_LOADING_AIRLINES', payload: false });
      } catch (error) {
        dispatch({ type: 'FETCH_FAILURE', payload: error });
      }
    };
    fetchData();
  }, [dispatch]);
};
