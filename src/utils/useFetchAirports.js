import { useEffect } from 'react';
import axios from 'axios';
import { useAppContext } from '../context/app-context';
import infoAirport from '../codeIata';

export const useFetchAirports = () => {
  const [, dispatch] = useAppContext();
  useEffect(() => {
    const fetchData = async () => {
      dispatch({ type: 'IS_LOADING_AIRPORTS', payload: true });
      try {
        const API_KEY = process.env.REACT_APP_API_KEY;
        const res = await axios({
          method: 'get',
          url: `https://recruitment.shippypro.com/flight-engine/api/airports/all`,
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${API_KEY}`,
          },
        });
        const fetchedAirports = res.data.data.map((key) => {
          return { id: key.id, codeIata: key.codeIata };
        });
        let newAirports = fetchedAirports.map((item, i) =>
          Object.assign({}, item, infoAirport[i]),
        );
        dispatch({ type: 'FETCH_AIRPORTS', payload: newAirports });
        dispatch({ type: 'IS_LOADING_AIRPORTS', payload: false });
      } catch (error) {
        dispatch({ type: 'FETCH_FAILURE', payload: error });
      }
    };
    fetchData();
  }, [dispatch]);
};
