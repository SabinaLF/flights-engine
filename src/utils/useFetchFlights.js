import { useEffect } from 'react';
import axios from 'axios';
import { useAppContext } from '../context/app-context';

export const useFetchFlights = () => {
  const [, dispatch] = useAppContext();
  useEffect(() => {
    const fetchData = async () => {
      dispatch({
        type: 'IS_LOADING_CHEAPEST_FLIGHTS',
        payload: true,
      });
      try {
        const API_KEY = process.env.REACT_APP_API_KEY;
        const res = await axios({
          method: 'get',
          url: `https://recruitment.shippypro.com/flight-engine/api/flights/all`,
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${API_KEY}`,
          },
        });
        const sortedFlights = res.data.data.sort((a, b) => {
          return a.price - b.price;
        });
        const cheapFlights = sortedFlights.slice(0, 3);
        dispatch({
          type: 'FETCH_CHEAPEST_FLIGHTS',
          payload: cheapFlights,
        });
        dispatch({
          type: 'IS_LOADING_CHEAPEST_FLIGHTS',
          payload: false,
        });
      } catch (error) {
        dispatch({ type: 'FETCH_FAILURE', payload: error });
      }
    };
    fetchData();
  }, [dispatch]);
};
