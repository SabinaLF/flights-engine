import React from 'react';
import styled from 'styled-components';
import { useAppContext } from './context/app-context';
import GlobalStyle from './components/GlobalStyle';
import Header from './components/Header';
import Search from './components/Search';
import FlightResult from './components/FlightResult';
import { useFetchAirports } from './utils/useFetchAirports';
import { useFetchAirlines } from './utils/useFetchAirlines';
import { useFetchFlights } from './utils/useFetchFlights';
import bg from './img/bg.jpg';

const Body = styled.div`
  @media (min-width: ${(props) => props.theme.breakpoint}) {
    display: flex;
    align-items: center;
    justify-content: center;
    background-image: url(${bg});
    background-repeat: no-repeat;
    background-size: cover;
    padding: 20rem 10rem;
    width: 100vw;
    height: 100vh;
  }
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  background-color: ${(props) => props.theme.colors.primaryBackground};
`;

const Content = styled.div`
  @media (min-width: ${(props) => props.theme.breakpoint}) {
    display: flex;
    flex-direction: row;
    @supports (display: grid) {
      display: grid;
      grid-template-columns: 33% 67%;
    }
  }
`;

const Loading = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 3rem;
  color: ${(props) => props.theme.colors.button};
`;

const App = () => {
  const [
    {
      airports,
      airlines,
      flights,
      isLoadingAirports,
      isLoadingAirlines,
      isLoadingCheapestFlights,
    },
  ] = useAppContext();
  useFetchAirlines();
  useFetchAirports();
  useFetchFlights();
  console.log(airports, airlines, flights);
  return (
    <>
      <GlobalStyle />
      <Body>
        <Wrapper>
          <Header />
          {!isLoadingAirports &&
          !isLoadingAirlines &&
          !isLoadingCheapestFlights ? (
            <Content>
              <Search />
              <FlightResult />
            </Content>
          ) : (
            <Loading>Loading...</Loading>
          )}
        </Wrapper>
      </Body>
    </>
  );
};

export default App;
