import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
*,
*::after,
*::before {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
    list-style-type: none;
    text-decoration: none;
}

html {
    // This defines what 1rem is
    font-size: 62.5%; //1 rem = 10px; 10px/16px = 62.5%
}

body {
    box-sizing: border-box;
    font-size: 20px;
    font-weight:bold;
    background-color: ${(props) => props.theme.colors.primaryBackground};
    font-family: 'Open Sans Condensed', Arial, sans-serif;
    max-height: 100vh;
    max-width: 100vw;
    position: relative;
    overflow-x: hidden;
}  
`;

export default GlobalStyle;
