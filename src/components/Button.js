import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.button`
  background-color: ${(props) => props.theme.colors.secondaryContent};
  color: ${(props) => props.theme.colors.secondaryBackground};
  height: 4rem;
  font-size: 2rem;
  border: none;
  opacity: 0.7;
  :hover,
  :focus {
    outline: 0;
    color: ${(props) => props.theme.colors.button};
    opacity: 1;
  }
`;

const Button = ({ onClick }) => {
  return <Wrapper onClick={onClick}>Search</Wrapper>;
};

export default Button;
