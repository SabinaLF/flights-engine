import React from 'react';
import styled from 'styled-components';
import { useAppContext } from '../context/app-context';
import Card from './Card';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-vertical: center;
  flex-direction: column;
  flex: 2;
`;

const Price = styled.div`
  color: ${(props) => props.theme.colors.button};
  font-size: 3rem;
  padding: 0 2rem;
  @media (min-width: ${(props) => props.theme.breakpoint}) {
    padding-left: 0;
  }
`;

const StyledPrice = styled(Price)`
  display: flex;
  justify-content: flex-end;
  font-size: 2rem;
`;

const SubTitle = styled.div`
  display: flex;
  justify-content: center;
  color: ${(props) => props.theme.colors.button};
  font-size: 3rem;
  padding: 0 2rem;
  @media (min-width: ${(props) => props.theme.breakpoint}) {
    padding-left: 0;
  }
`;

const CheapFlight = styled.div`
  margin-bottom: 2rem;
`;

const FlightResult = () => {
  const [
    {
      flights,
      cheapestFlights,
      airports,
      airlines,
      departureCode,
      arrivalCode,
    },
  ] = useAppContext();

  let totalPriceArray = [];

  const cheapestFlight = cheapestFlights.map((f) => {
    const departureCity = airports.filter((a) => a.id === f.departureAirportId);
    const arrivalCity = airports.filter((a) => a.id === f.arrivalAirportId);
    const airline = airlines.filter((l) => l.id === f.airlineId);

    return (
      <CheapFlight key={f.id}>
        <Card
          underlineDep={true}
          departureCity={departureCity[0].city}
          departureAirport={departureCity[0].nameAirport}
          airline={airline[0].name}
          underlineArr={true}
          arrivalCity={arrivalCity[0].city}
          arrivalAirport={arrivalCity[0].nameAirport}
        ></Card>
        <StyledPrice>&euro; {f.price.toFixed(2)}</StyledPrice>
      </CheapFlight>
    );
  });

  const flight = flights.map((f) => {
    const departureCity = airports.filter((a) => a.id === f.departureAirportId);
    const arrivalCity = airports.filter((a) => a.id === f.arrivalAirportId);
    const airline = airlines.filter((l) => l.id === f.airlineId);
    totalPriceArray.push(f.price);
    return (
      <Card
        key={f.id}
        underlineDep={departureCity[0].codeIata === departureCode}
        departureCity={departureCity[0].city}
        departureAirport={departureCity[0].nameAirport}
        airline={airline[0].name}
        underlineArr={arrivalCity[0].codeIata === arrivalCode}
        arrivalCity={arrivalCity[0].city}
        arrivalAirport={arrivalCity[0].nameAirport}
      ></Card>
    );
  });

  const totalPrice = totalPriceArray.reduce((total, amount) => {
    return total + amount;
  }, 0);

  if (cheapestFlights.length !== 0 && flights.length === 0) {
    return (
      <Wrapper>
        <SubTitle>Our cheapest flights</SubTitle>
        {cheapestFlight}
      </Wrapper>
    );
  }

  return (
    <Wrapper>
      {flight}
      {totalPrice !== 0 && <Price>&euro; {totalPrice.toFixed(2)}</Price>}
    </Wrapper>
  );
};

export default FlightResult;
