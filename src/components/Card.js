import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  padding: 0 2rem;
  @media (min-width: ${(props) => props.theme.breakpoint}) {
    padding-left: 0;
  }
`;

const FlightInfo = styled.div`
  display: flex;
  width: 100%;
  background-color: ${(props) => props.theme.colors.secondaryContentLight};
  border-bottom: 0.1rem solid ${(props) => props.theme.colors.primaryContent};
  margin-bottom: 0.3rem;
  padding: 0 2rem;
`;

const Info = styled.div`
  display: flex;
  flex-direction: column;
  width: 33%;
`;

const City = styled.div`
  color: ${(props) => props.theme.colors.primaryContent};
  text-decoration: ${(props) => (props.underline ? 'underline' : 'none')};
  display: flex;
  justify-content: center;
`;

const AirportName = styled.div`
  display: flex;
  justify-content: center;
  font-size: 1.4rem;
  color: ${(props) => props.theme.colors.secondaryContent};
`;

const Icon = styled.i`
  display: flex;
  justify-content: center;
  color: green;
  font-size: 3rem;
  color: ${(props) => props.theme.colors.button};
`;

const Airline = styled.div`
  display: flex;
  justify-content: center;
  font-size: 1.4rem;
  color: ${(props) => props.theme.colors.secondaryContent};
`;

const Card = ({
  underlineDep,
  departureCity,
  departureAirport,
  underlineArr,
  airline,
  arrivalCity,
  arrivalAirport,
}) => {
  return (
    <Wrapper>
      <FlightInfo>
        <Info>
          <City underline={underlineDep}>{departureCity}</City>
          <AirportName>{departureAirport}</AirportName>
        </Info>
        <Info>
          <Icon className="material-icons copyright-icon">arrow_right_alt</Icon>
          <Airline>{airline}</Airline>
        </Info>
        <Info>
          <City underline={underlineArr}>{arrivalCity}</City>
          <AirportName>{arrivalAirport}</AirportName>
        </Info>
      </FlightInfo>
    </Wrapper>
  );
};

export default Card;
