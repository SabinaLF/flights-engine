import React from 'react';
import styled from 'styled-components';
import { useAppContext } from '../context/app-context';

const Wrapper = styled.select`
  background-color: ${(props) => props.theme.colors.secondaryBackground};
  color: ${(props) => props.theme.colors.primaryContent};
  font-size: 2rem;
  height: 4rem;
  border: none;
  margin-bottom: 2rem;
  :focus {
    outline: none;
    border-bottom: 0.2rem solid ${(props) => props.theme.colors.primaryContent};
  }
`;
const Select = ({ type, onChange }) => {
  const [{ departureCode, arrivalCode, airports }] = useAppContext();
  let val;

  if (type === 'departureCity') {
    val = departureCode;
  } else {
    val = arrivalCode;
  }

  return (
    <Wrapper defaultValue={val} onChange={onChange}>
      {airports.map((airport) => (
        <option key={airport.id} value={airport.codeIata}>
          {`${airport.city} - ${airport.nameAirport}`}
        </option>
      ))}
    </Wrapper>
  );
};

export default Select;
