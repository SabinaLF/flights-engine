import React from 'react';
import styled from 'styled-components';
import axios from 'axios';
import { useAppContext } from '../context/app-context';
import Select from './Select';
import Button from './Button';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-vertical: center;
  flex: 1;
  flex-direction: column;
  margin-bottom: 2rem;
  padding: 2rem;
`;

const SubTitle = styled.div`
  display: flex;
  justify-content: center;
  color: ${(props) => props.theme.colors.button};
  font-size: 2rem;
  padding: 0 2rem;
  @media (min-width: ${(props) => props.theme.breakpoint}) {
    padding-left: 0;
  }
`;

const Label = styled.span`
  color: ${(props) => props.theme.colors.button};
`;

const Search = () => {
  const [
    { departureCode, arrivalCode, messageNoFlights },
    dispatch,
  ] = useAppContext();

  const onSelectDepartureCity = (e) => {
    dispatch({ type: 'SET_DEPARTURE_CODE', payload: e.target.value });
  };
  const onSelectArrivalCity = (e) => {
    dispatch({ type: 'SET_ARRIVAL_CODE', payload: e.target.value });
  };

  const fetchData = async (departureCity, arrivalCity) => {
    dispatch({ type: 'FETCH_FLIGHTS', payload: [] });
    if (departureCity !== arrivalCity) {
      dispatch({
        type: 'HANDLE_NO_FLIGHTS',
        payload: '',
      });
      try {
        const API_KEY = process.env.REACT_APP_API_KEY;
        const res = await axios({
          method: 'get',
          url: `https://recruitment.shippypro.com/flight-engine/api/flights/from/${departureCity}/to/${arrivalCity}`,
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${API_KEY}`,
          },
        });
        dispatch({ type: 'FETCH_CHEAPEST_FLIGHTS', payload: [] });
        dispatch({ type: 'FETCH_FLIGHTS', payload: res.data.data });
      } catch (error) {
        dispatch({ type: 'FETCH_FAILURE', payload: error });
      }
    } else
      dispatch({
        type: 'HANDLE_NO_FLIGHTS',
        payload: 'Please choose two different airports!',
      });
  };

  const handleSubmit = () => {
    fetchData(departureCode, arrivalCode);
  };

  return (
    <>
      <Wrapper>
        <Label>From:</Label>
        <Select type="departureCity" onChange={onSelectDepartureCity} />
        <Label>To:</Label>
        <Select type="arrivalCity" onChange={onSelectArrivalCity} />
        <Button onClick={handleSubmit}>Search</Button>
        {messageNoFlights !== '' && <SubTitle>{messageNoFlights}</SubTitle>}
      </Wrapper>
    </>
  );
};

export default Search;
