import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 5rem;
  padding: 1rem 0;
  color: ${(props) => props.theme.colors.primaryContent};
  @media (min-width: ${(props) => props.theme.breakpoint}) {
    justify-content: flex-start;
    padding-left: 2rem;
    align-items: center;
    font-size: 4rem;
  }
`;

const Header = () => {
  return <Wrapper>Find your flight</Wrapper>;
};

export default Header;
