import React from 'react';
import ReactDOM from 'react-dom';
import AppContextProvider from './context/app-context';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { ThemeProvider } from 'styled-components';
import { light } from '../src/theme';
import { initialState, reducer } from './reducers';

ReactDOM.render(
  <React.StrictMode>
    <AppContextProvider state={initialState} reducer={reducer}>
      <ThemeProvider theme={light}>
        <App />
      </ThemeProvider>
    </AppContextProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
