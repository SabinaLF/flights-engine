// prettier-ignore
export default function AppReducer(state, action) {
  switch (action.type) {
    case 'FETCH_AIRLINES': return {...state, airlines: action.payload};
    case 'FETCH_AIRPORTS': return {...state, airports: action.payload};
    case 'FETCH_CHEAPEST_FLIGHTS': return {...state, cheapestFlights: action.payload};
    case 'IS_LOADING_AIRLINES': return {...state, isLoadingAirlines: action.payload};
    case 'IS_LOADING_AIRPORTS': return {...state, isLoadingAirports: action.payload};
    case 'IS_LOADING_CHEAPEST_FLIGHTS': return {...state, isLoadingCheapestFlights: action.payload};
    case 'FETCH_FLIGHTS': return {...state, flights: action.payload};
    case 'SET_ARRIVAL_CODE': return {...state, arrivalCode: action.payload};
    case 'SET_DEPARTURE_CODE': return {...state, departureCode: action.payload};
    case 'FETCH_FAILURE': return {...state, error: action.payload};
    case 'HANDLE_NO_FLIGHTS': return {...state, messageNoFlights: action.payload};
    default: return state;
  }
}
