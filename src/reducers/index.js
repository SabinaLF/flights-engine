import AppReducer from './AppReducer';

export const initialState = {
  airports: [],
  airlines: [],
  flights: [],
  error: null,
  departureCode: 'PSA',
  arrivalCode: 'PSA',
  messageNoFlights: '',
  cheapestFlights: [],
  isLoadingAirports: true,
  isLoadingAirlines: true,
  isLoadingCheapestFlights: true,
};

export const reducer = (state, action) => AppReducer(state, action);
